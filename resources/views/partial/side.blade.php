<nav id="sidebar">
    <div class="p-4 pt-5">
        <a
            href="#"
            class="img logo rounded-circle mb-5"
            style="background-image: url(images/logo.jpg)"
        ></a>
        <ul class="list-unstyled components mb-5">
            <li>
                <a href="/">Home</a>
            </li>
            <li>
                <a href="/my-question">My Question</a>
            </li>
            <li>
                <a href="/profile">Profile</a>
            </li>
            <li>
                <a href="#">Logout</a>
            </li>
        </ul>

        <div class="footer">
            <p>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;
                <script>
                    document.write(new Date().getFullYear());
                </script>
                All rights reserved | This template is made with
                <i class="icon-heart" aria-hidden="true"></i> by
                <a href="https://colorlib.com" target="_blank"
                    >Colorlib.com</a
                >
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
        </div>
    </div>
</nav>