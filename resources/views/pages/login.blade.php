<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('login-template/fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{asset('login-template/css/owl.carousel.min.css')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('login-template/css/bootstrap.min.css')}}">
    
    <!-- Style -->
    <link rel="stylesheet" href="{{asset('login-template/css/style.css')}}">

    <title>Login #7</title>
  </head>
  <body>
  

  
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <img src="login-template/images/undraw_remotely_2j6y.svg" alt="Image" class="img-fluid">
        </div>
        <div class="col-md-6 contents">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="mb-4">
              <h3>Sign In</h3>
              <p class="mb-4">Don't have an account? <a href="/register" class="text-info">Register</a></p>
            </div>
            <form action="#" method="post">
              <div class="form-group first">
                <label for="email">Username</label>
                <input type="email" class="form-control" name="email" id="email">

              </div>
              <div class="form-group last mb-4 mt-1">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password">
                
              </div>

              <input type="submit" value="login" name="login" class="btn btn-block btn-primary">


            </form>
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
  </div>

  
    <script src="{{asset('login-template/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('login-template/js/popper.min.js')}}"></script>
    <script src="{{asset('login-template/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('login-template/js/main.js')}}"></script>
  </body>
</html>