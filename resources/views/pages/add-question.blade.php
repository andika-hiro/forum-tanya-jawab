@extends('layouts.master-template')

@section('page-title')
    Tanyain Dong!!
@endsection

@section('content')
<div class="container">
    <div class="row gutters">
    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
    <div class="card h-100">
        <div class="card-body">
            <div class="row gutters">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <h6 class="mt-3 mb-2 text-primary">Add Question</h6>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <div class="form-group">
                        <label for="Address">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="title">
                    </div>
                </div>
                </div>
                <div class="row gutters ">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 ml-2">
                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea name="body" id="body" cols="70" rows="5"></textarea>
                    </div>
                </div>
                </div>
                <div class="row gutters ">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                        <button type="button" class="btn btn-success ml-2"><i class="fa fa-plus-circle mr-3" aria-hidden="true"></i>Image</button>
                    </div>
                </div>
            <div class="row gutters ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                    <div class="text-center ">
                        <button type="button" id="submit" name="submit" class="btn btn-primary ">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>


@endsection
