@extends('layouts.master-template')

@section('page-title')
    Tanyain Dong!!
@endsection

@section('content')
<div class="header-content d-flex">
    <h2 class="mb-4">My Question</h2>
    <div class="addquestion pt-1">
        <button type="button" class="btn btn-success ml-4 h-50"><i class="fa fa-plus-circle mr-3" aria-hidden="true"></i>Add Question</button>
    </div>

</div>


<div class="questionContainer">
    <div class="question d-flex border-top border-bottom">
        <div class="answerCount pt-5">
            <p>2 answers</p>
        </div>
        <div class="bodyQuestion ml-5 w-50 mt-3">
            <div class="title">
                <a href="/question"><h3>Contoh Title</h3></a>
            </div>
            <div class="categoryCont w-25">
                <div class="Category">
                    <p class="rounded border w-auto d-flex justify-content-center bg-info text-white">python</p>
                </div>
            </div>
            <div class="user d-flex justify-content-end">
                <i class="fa fa-users pt-1" aria-hidden="true"></i>
                <p class="ml-2">Andika Hiro</p>
            </div>
        </div>
    
    </div>
    <div class="question d-flex border-top border-bottom mb-2">
        <div class="answerCount pt-5">
            <p>2 answers</p>
        </div>
        <div class="bodyQuestion ml-5 w-50 mt-3">
            <div class="title">
                <h3>Contoh Title</h3>
            </div>
            <div class="categoryCont w-25">
                <div class="Category">
                    <p class="rounded border w-auto d-flex justify-content-center bg-info text-white">python</p>
                </div>
            </div>
            <div class="user d-flex justify-content-end">
                <i class="fa fa-users pt-1" aria-hidden="true"></i>
                <p class="ml-2">Andika Hiro</p>
            </div>
        </div>
    </div>
    </div>
@endsection