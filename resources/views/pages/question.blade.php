@extends('layouts.master-template')

@section('page-title')
    Tanyain Dong!!
@endsection

@section('content')
<div class="header-content d-flex">
    <h2 class="mb-4">Question</h2>
</div>

<div class="questionContainer">
    <div class="title border-bottom">
        <h3>Judul Pertanyaan</h3>
    </div>
    <div class="subject">
        <h5>Ini merupakan Subject Dummy Yang dibuat untuk mengetes semuanya, saya ingin bertanyqa tentang semua hal yang ada di dunia ini</h5>
    </div>
    <h2 class="mt-5">Answer</h2>
    <div class="answer-container d-flex flex-column">
        <textarea name="answer-question" id="" cols="100" rows="10"></textarea>
        <button type="button" class="btn btn-success">Submit</button>
    </div>

</div>

@endsection