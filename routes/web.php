<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/question', function () {
    return view('pages.question');
});

Route::get('/my-question', function () {
    return view('pages.my-question');
});

Route::get('/profile', function () {
    return view('pages.profile');
});

Route::get('/add-question', function () {
    return view('pages.add-question');
});

Route::get('/login', function () {
    return view('pages.login');
});

Route::get('/register', function () {
    return view('pages.register');
});
